package cz.tmckdevelopment.thefuntasty.model;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Tomáš Vítek on 01.07.2016 at 11:43.
 */

/*
 * This object is used to save informations about
 * State. Data are loaded from splashScreen
 */
public final class State implements Serializable {

    private ArrayList<Float> latlng;
    private String name, capital, region;
    private int population;

    public ArrayList<Float> getLatlng() {
        return latlng;
    }

    public String getName() {
        return name;
    }

    public String getCapital() {
        return capital;
    }

    public String getRegion() {
        return region;
    }

    public int getPopulation() {
        return population;
    }
}
