package cz.tmckdevelopment.thefuntasty.net;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;

import java.util.Map;

/**
 * Created by Tomas on 16.08.2016.
 */
public final class JsonDownloader {

    //Declare Objects
    private static JsonArrayRequest sJsonArrayRequest;
    private static RequestQueue sRequestQueue;
    private static Context sContext;
    private static final String BASE_URL = "https://restcountries.eu/rest/v1/";

    /*
    This method will add url to BASE_URL
     */
    private static String getFullUrl(String url) {
        return BASE_URL + url;
    }

    /*
    Method will get data
     */
    public static void get(String url, Context context, Response.Listener listener, Response.ErrorListener errorListener, final Map<String, String> params) {
        downloadJson(Request.Method.GET, url, context, listener, errorListener, params);
    }

    /*
    Method will post data
   */
    public static void post(String url, Context context, Response.Listener listener, Response.ErrorListener errorListener, final Map<String, String> params) {
        downloadJson(Request.Method.POST, url, context, listener, errorListener, params);
    }

    /*
    Will post or get data
     */
    private static void downloadJson(int method, String url, Context context, Response.Listener listener, Response.ErrorListener errorListener, final Map<String, String> params) {
        sContext = context;
        sRequestQueue = Volley.newRequestQueue(context);
        sJsonArrayRequest = new JsonArrayRequest(method, getFullUrl(url), listener, errorListener) {
            @Override
            protected Map<String, String> getParams() {
                return params;
            }
        };
        sRequestQueue.add(sJsonArrayRequest);
    }

    /*
     Will show info Toast about error
     */
    public static void showError(VolleyError error) {

        if (error instanceof TimeoutError || error instanceof NoConnectionError) {
            Toast.makeText(sContext, "No internet connection", Toast.LENGTH_SHORT).show();
        } else if (error instanceof AuthFailureError) {
            Toast.makeText(sContext, "AuthFailure Error", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ServerError) {
            Toast.makeText(sContext, "Server Error", Toast.LENGTH_SHORT).show();
        } else if (error instanceof NetworkError) {
            Toast.makeText(sContext, "Network Error", Toast.LENGTH_SHORT).show();
        } else if (error instanceof ParseError) {
            Toast.makeText(sContext, "Parse Error", Toast.LENGTH_SHORT).show();
        }
    }

}
