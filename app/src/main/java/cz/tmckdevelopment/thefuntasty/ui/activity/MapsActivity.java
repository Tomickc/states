package cz.tmckdevelopment.thefuntasty.ui.activity;

import android.os.Bundle;

import cz.tmckdevelopment.thefuntasty.ui.base.BaseActivity;
import cz.tmckdevelopment.thefuntasty.ui.fragment.MapsFragment;

/**
 * Created by Tomas on 03.08.2016.
 */
public final class MapsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Load MapsFragment on screen
        showFragment(new MapsFragment(), savedInstanceState);
    }
}