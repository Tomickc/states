package cz.tmckdevelopment.thefuntasty.ui.activity;

import android.os.Bundle;

import cz.tmckdevelopment.thefuntasty.R;
import cz.tmckdevelopment.thefuntasty.ui.base.BaseActivity;
import cz.tmckdevelopment.thefuntasty.ui.fragment.MapsFragment;
import cz.tmckdevelopment.thefuntasty.ui.fragment.StatesFragment;
import cz.tmckdevelopment.thefuntasty.util.Utils;

/**
 * Created by Tomas on 31.07.2016.
 */
public final class StatesActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.panes);

        //Check if device is tablet or mobile
        if (findViewById(R.id.mobileLayout) != null) {
            Utils.setTablet(false);
            showFragment(new StatesFragment(), savedInstanceState);
        } else {
            Utils.setTablet(true);
            showFragment(new StatesFragment(), R.id.frameStates, savedInstanceState);
            showFragment(new MapsFragment(), R.id.frameMap, savedInstanceState);

        }
    }
}