package cz.tmckdevelopment.thefuntasty.ui.adapter;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import cz.tmckdevelopment.thefuntasty.R;
import cz.tmckdevelopment.thefuntasty.model.State;

/**
 * Created by Tomáš Vítek on 01.07.2016 at 11:42.
 */

public final class StatesAdapter extends ArrayAdapter<State> {

    //Declare variables
    private final Activity mContext;
    private final List<State> mStateList;


    //Set constructor
    public StatesAdapter(Activity context, List<State> stateList) {
        super(context, R.layout.listview_item_state, stateList);

        this.mContext = context;
        this.mStateList = stateList;
    }

    //Set View
    public View getView(int position, View view, ViewGroup parent) {

        View itemView = mContext.getLayoutInflater().inflate(R.layout.listview_item_state, null, true);
        ((TextView) itemView.findViewById(R.id.textViewStateName)).setText(mStateList.get(position).getName());

        return itemView;
    }
}