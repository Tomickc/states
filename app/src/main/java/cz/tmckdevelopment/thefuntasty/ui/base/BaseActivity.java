package cz.tmckdevelopment.thefuntasty.ui.base;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Tomas on 28.07.2016.
 */
public abstract class BaseActivity extends AppCompatActivity {

    /*
     Show fragment in current activity
     */
    public void showFragment(Fragment fragment, Bundle savedInstanceState) {
        showFragment(android.R.id.content,fragment,savedInstanceState);
    }

    /*
     When you dont want to change screen fragment but only frame
     you can do it by define param in this method
     */
    public void showFragment(Fragment fragment, int containerView, Bundle savedInstanceState) {
        showFragment(containerView,fragment,savedInstanceState);
    }

    /*
    Only change place where will be fragment replaced
     */
    private void showFragment(int content, Fragment fragment, Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(content, fragment);
            fragmentTransaction.commit();
        }
    }

}
