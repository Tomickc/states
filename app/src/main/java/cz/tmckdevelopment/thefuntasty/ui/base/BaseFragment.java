package cz.tmckdevelopment.thefuntasty.ui.base;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

/**
 * Created by Tomas on 01.08.2016.
 */
public abstract class BaseFragment extends Fragment {


    /*
     When you dont want to change screen fragment but only frame
     you can do it by define param in this method
     */
    public void showFragment(Fragment fragment,int containerView) {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.replace(containerView, fragment);
        fragmentTransaction.commit();
    }

    /*
     When you want to open new activity just use this method
     */
    public void showActivity(Class nextClass){
        startActivity(new Intent(getActivity(),nextClass));
        getActivity().overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
    }
}
