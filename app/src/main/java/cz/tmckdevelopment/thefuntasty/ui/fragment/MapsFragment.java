package cz.tmckdevelopment.thefuntasty.ui.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import cz.tmckdevelopment.thefuntasty.R;
import cz.tmckdevelopment.thefuntasty.model.State;
import cz.tmckdevelopment.thefuntasty.ui.base.BaseFragment;
import cz.tmckdevelopment.thefuntasty.util.Utils;

public final class MapsFragment extends BaseFragment implements OnMapReadyCallback {

    //Declare variables
    private static GoogleMap sMap;
    private static State sCurrentState;
    private static View sWindow;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_maps, container, false);
        sWindow = getActivity().getLayoutInflater().inflate(R.layout.info_window_map, null);

        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        return v;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        sMap = googleMap;

        if (Utils.getsStatesOnMap() != null) {
            showMarker();
        }
    }

    public static void showMarker() {

        if (sMap != null) {

            //Get current state
            sCurrentState = Utils.getsStatesOnMap().get(Utils.getPosition());
            //Current state position
            LatLng state = new LatLng(sCurrentState.getLatlng().get(0), sCurrentState.getLatlng().get(1));

            //Map settings
            sMap.moveCamera(CameraUpdateFactory.newLatLngZoom(state, 4));
            sMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
            sMap.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET)).position(state));

            //When marker is clicked show this window
            sMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                @Override
                public View getInfoWindow(Marker arg0) {
                    return null;
                }

                @Override
                public View getInfoContents(Marker marker) {
                    ((TextView) sWindow.findViewById(R.id.textViewStateTitle)).setText(sCurrentState.getName());
                    ((TextView) sWindow.findViewById(R.id.textViewInfo)).setText("Capital city: " + sCurrentState.getCapital() + "\nRegion: " + sCurrentState.getRegion() + "\nPopulation: " + Utils.editNumber(sCurrentState.getPopulation()));
                    return sWindow;
                }
            });
        }
    }
}
