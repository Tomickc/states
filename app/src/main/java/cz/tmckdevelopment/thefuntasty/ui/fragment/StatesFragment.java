package cz.tmckdevelopment.thefuntasty.ui.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

import cz.tmckdevelopment.thefuntasty.R;
import cz.tmckdevelopment.thefuntasty.model.State;
import cz.tmckdevelopment.thefuntasty.net.JsonDownloader;
import cz.tmckdevelopment.thefuntasty.ui.activity.MapsActivity;
import cz.tmckdevelopment.thefuntasty.ui.adapter.StatesAdapter;
import cz.tmckdevelopment.thefuntasty.ui.base.BaseFragment;
import cz.tmckdevelopment.thefuntasty.util.Utils;

/**
 * Created by Tomas on 30.07.2016.
 */
public final class StatesFragment extends BaseFragment {

    //Declare Objects and Views
    private ImageView mImageViewBackground;
    private ListView mStatesListView;
    private EditText mFindStateEditText;
    private ArrayList<State> mFindingArrayList;
    private ArrayList<State> mStates;
    private SharedPreferences mSharedPreferences;
    private FrameLayout mTopLine;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_states, container, false);
        initUI(v);

        //Check if states are already loaded
        if (Utils.getStates() == null) {
            getStates();
        } else {
            setUI();
        }
        return v;
    }

    /*
     will define views
     */
    private void initUI(View view) {
        mImageViewBackground = (ImageView) view.findViewById(R.id.imageViewBackground);
        mStatesListView = (ListView) view.findViewById(R.id.listViewStates);
        mFindStateEditText = (EditText) view.findViewById(R.id.editTextFindState);
        mFindingArrayList = new ArrayList<>();
        mSharedPreferences = getActivity().getPreferences(Context.MODE_PRIVATE);
        mTopLine = (FrameLayout) view.findViewById(R.id.topLine);

        //Make views components
        mStatesListView.setAlpha(0.0f);
        mFindStateEditText.setAlpha(0.0f);
        mTopLine.setAlpha(0.0f);

        //show background
        Glide.with(getActivity()).load(R.drawable.background).centerCrop().into(mImageViewBackground);
    }

    /*
     Will set statesListView
     */
    private void setListView(StatesAdapter statesAdapter, final ArrayList<State> statesArrayList) {

        mStatesListView.setAdapter(statesAdapter);
        mStatesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Utils.setPosition(position);
                Utils.setsStatesOnMap(statesArrayList);

                if (Utils.isTablet()) {
                    showFragment(new MapsFragment(), R.id.frameMap);
                } else {
                    showActivity(MapsActivity.class);
                }
            }
        });
        mStatesListView.animate().alpha(1);
    }

    /*
     will load downloaded datas into listView
     */
    private void setUI() {

        //Show components
        mFindStateEditText.animate().alpha(1);
        mTopLine.animate().alpha(1);

        //First load all states
        if (Utils.getStates() != null) {
            setListView(new StatesAdapter(getActivity(), Utils.getStates()), Utils.getStates());
            mFindStateEditText.setHint("Celkem " + String.valueOf(Utils.getStates().size()) + " států");
        }


        //When user try to find state we have to use textChangeListener
        mFindStateEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                //If user typed some letters we clear arrayList and check if typed word is in arraylist than we will save same words into another arrayList
                mFindingArrayList.clear();

                for (int i = 0; i < Utils.getStates().size(); i++) {
                    if (Utils.getStates().get(i).getName().toLowerCase().contains(mFindStateEditText.getText().toString().toLowerCase())) {
                        mFindingArrayList.add(Utils.getStates().get(i));
                    }
                }

                setListView(new StatesAdapter(getActivity(), mFindingArrayList), mFindingArrayList);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    /*
  Will download datas from JSONArray (Saved in sharedPrefs or downloaded)
  and will set to State Object and array with this objects will be saved into Utils.class
  and after delay time will show statesList
  */
    private void downloadGson(JSONArray timeline) {
        mStates = new ArrayList<>();
        Gson gson = new Gson();
        for (int i = 0; i < timeline.length(); i++) {
            State state = null;
            try {
                state = gson.fromJson(timeline.getJSONObject(i).toString(), State.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mStates.add(state);

        }
        Utils.setStates(mStates);
        setUI();

        if (Utils.isTablet()) {
            Utils.setsStatesOnMap(mStates);
            MapsFragment.showMarker();
        }

    }

    /*
     Will try to download JsonArray from current URL and save it into sharedPrefs or
     will show error Toast and will load last saved datas
     */
    private void getStates() {
        JsonDownloader.get("all", getActivity(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                downloadGson(response);

                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putString("timeline", response.toString());
                editor.apply();
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                try {
                    downloadGson(new JSONArray(mSharedPreferences.getString("timeline", "")));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                JsonDownloader.showError(error);
            }
        }, null);
    }
}
