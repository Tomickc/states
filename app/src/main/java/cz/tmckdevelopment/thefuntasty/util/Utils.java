package cz.tmckdevelopment.thefuntasty.util;

import java.util.ArrayList;

import cz.tmckdevelopment.thefuntasty.model.State;

/**
 * Created by Tomas on 28.07.2016.
 */
public final class Utils {

    private static ArrayList<State> sStates,sStatesOnMap;
    private static int sPosition;
    private static boolean sTablet;

    public static void setsStatesOnMap(ArrayList<State> sStatesOnMap) {
        Utils.sStatesOnMap = sStatesOnMap;
    }

    public static ArrayList<State> getsStatesOnMap() {
        return sStatesOnMap;
    }

    /*
     Will return edited StringBuffer because long numbers are hard to read
     so this method will make them readable
     */
    public static StringBuffer editNumber(int population){
        StringBuffer populationString = new StringBuffer(String.valueOf(population));

        for (int i = populationString.length()-3; i > 0; i -= 3) {
            populationString.insert(i," ");
        }
        return populationString;
    }

    public static void setTablet(boolean tablet) {
        Utils.sTablet = tablet;
    }

    public static boolean isTablet() {
        return sTablet;
    }

    public static int getPosition() {
        return sPosition;
    }

    public static ArrayList<State> getStates() {
        return sStates;
    }

    public static void setStates(ArrayList<State> states) {
        Utils.sStates = states;
    }

    public static void setPosition(int position) {
        Utils.sPosition = position;
    }

}
